from django import forms
from .models import Booking

class BookingForm(forms.ModelForm):
	class Meta:
		model = Booking
		fields = [
			'orderer',
			'email',
			'phone',
		]
		widgets = {
			'orderer' : forms.TextInput(attrs={'class': 'form-control', 'placeholder' : "Your name"}),
			# 'doctor' : forms.TextInput(
			# 	attrs={
			# 		'class' : 'form-control form-control-plaintext',
			# 		'readonly' : 'readonly',
			# 	}
			# ),
			# 'time' : forms.TextInput(
			# 	attrs={
			# 		'class' : 'form-control form-control-plaintext',
			# 		'readonly' : 'readonly',
			# 	}
			# ),
			'email' : forms.EmailInput(attrs={'class' : 'form-control', 'placeholder' : "Your email"}),
			'phone' : forms.TextInput(attrs={'class' : 'form-control'})
		}
	error_messages = {
		'required' : 'Please fill this field'
	}
