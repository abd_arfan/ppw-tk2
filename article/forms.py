from .models import *
from django import forms

class CommentNewsForm(forms.ModelForm):
    class Meta:
        model = CommentsNews
        fields = ('comment',)


class CommentTipsAndTrickForm(forms.ModelForm):
    class Meta:
        model = CommentsTipsAndTrick
        fields = ('comment',)