from django.db import models

class Doctor(models.Model):
	profile_picture = models.ImageField(upload_to='Profile_Picture/', blank=True)
	name = models.CharField(max_length=50)
	degree_name = models.CharField(max_length=80)
	message = models.TextField()
	known_for = models.TextField()
	path_to = models.TextField()
	passion = models.TextField()
	fav_quote = models.TextField()
	fav_quote_by = models.CharField(max_length=50)